package com.bdd.Runner;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.TestNG;
import org.testng.annotations.DataProvider;
import io.cucumber.testng.AbstractTestNGCucumberTests;
//@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:cucumber/Order.feature",
        //features = "classpath:cucumber",
        // features = "@target/rerun.txt",
        glue = "com.bdd.StepDefs",
       //tags = {"@ChangeFirstName"},
        dryRun = false,
        monochrome = true,
        plugin = {"pretty", "html:target/cucumber-report/com.test",
                "junit:target/cucumber-report/Cucumber.xml",
                "json:target/cucumber-report/Cucumber.json",
                "rerun:target/rerun.txt"}
                )

public class TestRunner extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
