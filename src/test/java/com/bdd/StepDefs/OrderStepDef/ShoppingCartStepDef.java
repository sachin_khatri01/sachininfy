package com.bdd.StepDefs.OrderStepDef;

import com.bdd.StepDefs.BaseStepDef;
import com.bdd.Webpages.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import com.bdd.Util.Log;

import java.io.IOException;

public class ShoppingCartStepDef extends BaseStepDef{
    private WebDriver driver;
    private String reference = "";
    private String username=null;




    @And("i verify order details on order confirmation page")
    public void iVerifyOrderDetailsOnOrderConfirmationPage() throws IOException {

        Boolean result=true;

        if(!result) {
            Assert.fail("Not able to update first name");
        }
      //  Log.info("Able to save the updated info");
    }



    @Given("i am on the page where a product can be added to cart for a signed user")
    public void iAmOnThePageWhereAProductCanBeSearchedAndAddedToCart() {

      //  Log.info("Opening the browser");

            Assert.assertTrue(true);

    }



    @When("i add \"([^\"]*)\" in the cart")
    public void iAddInTheCart(String Product)
    {
        Boolean result=true;

        if(!result)
        {
            Assert.fail("Could not click proceed to checkout");
        }

    }





    @When("i order a \"([^\"]*)\"")
    public void iOrderA(String product) throws IOException {
        Boolean result=true;

        if(!result)
        {

            Assert.fail("Could notconfirm order for"+product);
        }
      //  Log.info("Able to click confirm order on payments page");
    }


}
